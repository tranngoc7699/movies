var page = 1;
var pagerv = 1;
var querry;
var apikey = "daf94da856375d0b3b875be147c85d70";
const api = `https://api.themoviedb.org/3/search/movie?api_key=${apikey}`
async function evtSubmit(e)
{

  $(`#status`).empty();
  $('#pagelist').empty();     
  $('#review').empty(); $('#reviewBody').empty();

  page = 1;
  e.preventDefault();
  try {
  querry = $('form[id="moviessrc"]').find('input').val();
  if (querry!="")
  {
  loading();
  const reqStr =  `${api}&query=${querry}&page=${page}`; 
  const response = await fetch(reqStr); 
  const rs = await response.json();   
  $(`#status`).append(`Search : ${querry} found ${rs.total_pages} pages with ${rs.total_results} results`);
  fillMovies(rs.results);
  var totalpage = rs.total_pages;
  loadPage(totalpage);
  }
  else{
    $('#main').empty();
    $(`#status`).append(`You must type a movie to search`); 
  }
  }
  catch(err)
  {
    $('#main').empty();
  $(`#status`).append(`404. Somethings was wrong`); 
  }
}

async function actorMoviesSearch(e){
    $('#pagelist').empty();     
  $('#review').empty(); $('#reviewBody').empty(); 
  $(`#status`).empty();  
  page = 1;
  e.preventDefault();
  try 
  {
  querry = $('form[id="actorsrc"]').find('input').val();
  if (querry!="")
  {
    loading();
  const reqStr =  `https://api.themoviedb.org/3/search/person?api_key=${apikey}&query=${querry}&language=en-US&page=${page}`;
  const response = await fetch(reqStr); 
  const rs = await response.json();
  const act = `https://api.themoviedb.org/3/person/${rs.results[0].id}?api_key=${apikey}`;
  const actresp = await fetch(act); 
  const actrs = await actresp.json();
  $(`#status`).append(`Best Match : ${actrs.name} with ${rs.results[0].known_for.length} results`);
  fillMovies(rs.results[0].known_for);
  }
  else
  {    
    $('#main').empty();
    $(`#status`).append(`You must type a actor to search`); 
  }
  }
  catch (err)
  {
    $('#main').empty(); 
    $(`#status`).append(`404. Somethings was wrong`);    
  }
}

async function goToRVpage(x,id) {
  pagerv = x;
  const apiReview = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${apikey}&language=en-US&page=${pagerv}`;
  const rvs = await fetch(apiReview); 
    const rv = await rvs.json();
    fillReview(rv);
  loadPageRV(totalpage,id);
};

function loading()
{
  $('#main').empty();
  $('#main').append(`
  <div style ="margin : auto">
<div class="spinner-grow text-primary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-secondary" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-success" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-warning" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-info" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-light" role="status">
  <span class="sr-only">Loading...</span>
</div>
<div class="spinner-grow text-dark" role="status">
  <span class="sr-only">Loading...</span>
</div>
  </div>
  `);
}
function loadPageRV(totalpage,id)
{
  function fillPage(pageList, x,id)
  {
    var temp = x;
    if (x == "<<")
    {
      temp = pagerv -1;
    }
    if (x == ">>")
    {
      temp = pagerv + 1;
    }
    if (x != pagerv) 
    {
      pageList.append(`
      <li class="page-item">
      <a onClick="goToRVpage(${temp},${id})" class="page-link" href="#">${x}</a>
      </li>`);
    }
    else{
      
    pageList.append(`
    <li class="page-item">
    <a onClick="goToRVpage(${temp},${id})" class="page-link cur" href="#">${x}</a>
    </li>`);
    }
  }
  var pageList = $('#rvPage');
  pageList.empty();
  fillPage(pageList, "<<",id);
  if (totalpage <=40)
  {
    for (var x = 1;x <= totalpage;x++)
    {
      fillPage(pageList, x,id);
    }
  }
  else
  {
    if (pagerv < 10)
    {      
      for (var x = pagerv;x <= pagerv + 5;x++)
      {
        fillPage(pageList, x,id);
      }
      fillPage(pageList,"...");
      for (var x = parseInt(totalpage/2) -3;x <= parseInt(totalpage/2) + 3 + 5;x++)
      {
        fillPage(pageList, x,id);
      }
      fillPage(pageList,"...");
      for (var x = totalpage-5;x <= totalpage;x++)
      {
        fillPage(pageList, x,id);
      }      
    }
    else
    {
      
      for (var x = 1;x <=  5;x++)
      {
        fillPage(pageList, x,id);
      }
      fillPage(pageList,"...",id);
      if (totalpage - page < 10)
      {
        for (var x = pagerv-3;x <= pagerv+3;x++)
        {
          fillPage(pageList, x,id);
        }
        fillPage(pageList,"...");
        for (var x = totalpage-5;x <= totalpage;x++)
        {
          fillPage(pageList, x,id);
        }        
      }
      else
      {
        for (var x = parseInt(totalpage/2) -3;x <= parseInt(totalpage/2) + 3 + 5;x++)
        {
          fillPage(pageList, x,id);
        }
        fillPage(pageList,"...");
        for (var x = totalpage-5;x <= totalpage;x++)
        {
          fillPage(pageList, x,id);
        }  
      }
    }
  }
      fillPage(pageList, ">>",id);
}

function loadPage(totalpage)
{
  function fillPage(pageList, x)
  {
    var temp = x;
    if (x == "<<")
    {
      temp = page -1;
    }
    if (x == ">>")
    {
      temp = page + 1;
    }
    if (x != page) 
    {
      pageList.append(`
      <li class="page-item">
      <a onClick="goToPage(${temp})" class="page-link" href="#">${x}</a>
      </li>`);
    }
    else{
      
    pageList.append(`
    <li class="page-item">
    <a onClick="goToPage(${temp})" class="page-link cur" href="#">${x}</a>
    </li>`);
    }
  }
  var pageList = $('#pagelist');
  pageList.empty();
  fillPage(pageList, "<<");
  if (totalpage <=40)
  {
    for (var x = 1;x <= totalpage;x++)
    {
      fillPage(pageList, x);
    }
  }
  else
  {
    if (page < 10)
    {      
      for (var x = page;x <= page + 5;x++)
      {
        fillPage(pageList, x);
      }
      fillPage(pageList,"...");
      for (var x = parseInt(totalpage/2) -3;x <= parseInt(totalpage/2) + 3 + 5;x++)
      {
        fillPage(pageList, x);
      }
      fillPage(pageList,"...");
      for (var x = totalpage-5;x <= totalpage;x++)
      {
        fillPage(pageList, x);
      }      
    }
    else
    {
      
      for (var x = 1;x <=  5;x++)
      {
        fillPage(pageList, x);
      }
      fillPage(pageList,"...");
      if (totalpage - page < 10)
      {
        for (var x = page-3;x <= page+3;x++)
        {
          fillPage(pageList, x);
        }
        fillPage(pageList,"...");
        for (var x = totalpage-5;x <= totalpage;x++)
        {
          fillPage(pageList, x);
        }        
      }
      else
      {
        for (var x = parseInt(totalpage/2) -3;x <= parseInt(totalpage/2) + 3 + 5;x++)
        {
          fillPage(pageList, x);
        }
        fillPage(pageList,"...");
        for (var x = totalpage-5;x <= totalpage;x++)
        {
          fillPage(pageList, x);
        }  
      }
    }
  }
      fillPage(pageList, ">>");
}
async function loadTrend()
{
  const trend = "https://api.themoviedb.org/3/trending/all/day?api_key=daf94da856375d0b3b875be147c85d70";
  const response = await fetch(trend);
  loading();
  const rs = await response.json();
  fillMovies(rs.results);
}
function fillMovies(ms)
{
  $('#review').empty(); $('#reviewBody').empty();
  $('#main').empty();
  for (const x of ms)
  {
    $('#main').append(`
    <div class="col-md-4 py-1">    
                        <div class="card h-100" a href ="" onClick="evtChooseMovie(${x.id})">
                        <img src="http://image.tmdb.org/t/p/original${x.poster_path}" class="card-img-top" alt="${x.original_title}"/>
                        <div class="card-body">
                            <h5 class="card-title">${x.original_title}</h5>
                            <p class="card-text"><b>Release: </b> ${x.release_date}</p>
                            <p class="card-text"><b>Rate   : </b> ${x.vote_average}</p>
                            <p class="card-text"><b>Languge: </b> ${x.original_language}</p>
                        </div>
                    </div>
                </div>
    `);
  }
  
}
async function evtChooseMovie(movie_id)
{
    pagerv = 1;
     $('#pagelist').empty();     
     $('#review').empty(); $('#reviewBody').empty();
    var status = $(`#status`);
    const apiMovie = `https://api.themoviedb.org/3/movie/${movie_id}?api_key=${apikey}&language=en-US`;    
    const credit = `https://api.themoviedb.org/3/movie/${movie_id}/credits?api_key=${apikey}`;
    const apiReview = `https://api.themoviedb.org/3/movie/${movie_id}/reviews?api_key=${apikey}&language=en-US&page=${pagerv}`;
    console.log(apiReview);
    loading();
    const mv = await fetch(apiMovie); 
    const cr = await fetch(credit); 
    const rvs = await fetch(apiReview); 
    const rv = await rvs.json();
    const rsMv = await mv.json();
    const rsCr = await cr.json();
    status.empty();    
    status.append(rsMv.original_title);
    moviesDetail(rsMv,rsCr,rv);
    loadPageRV(rv.total_pages,movie_id);
}
function findActor_Director(credit,strActor,strDirector)
{
  strActor.value = "";
  strDirector.value = "";
    var actor =[], director=[];
    var temp = 0;
    for (const x of credit.cast)
    {
        if (temp > 5)
        {
          break;
        }
        actor.push(x);
        temp++;
    }
    for (const y of credit.crew)
    {
      if (y.job == "Director")
      {
        director.push(y);
      }
    }
    
  for (var i = 0; i < actor.length; i++) { 
    strActor.value += `<p class="card-text" style="margin-left:20px"> <a href="#" onClick="evtGetActor(${actor[i].id})" class="card-text">${actor[i].name}</a> cast ${actor[i].character}</p> `;
    }strActor.value += `<p class="card-text" style="text-align:center">.....</p>`;
  for (var i = 0; i < director.length; i++) {
    if (i != director.length-1 )
    {
      strDirector.value += `${director[i].name}, `;
    }
    else
    {      
      strDirector.value += `${director[i].name} `;
    }
    }
  
}

function moviesDetail(dt,credit,review)
{
  var strActor = {};
  var strDirector = {};
  var gene = "";
  for (i = 0; i < dt.genres.length;i++)
  {
    gene +=dt.genres[i].name;
    gene += ", ";
  }
  console.log(gene);
  findActor_Director(credit,strActor,strDirector);  
  $('#main').empty();
  $('#review').empty(); $('#reviewBody').empty();  
  $('#main').append(`
  <div class="card border-info mb-3" style="margin: auto; padding : 5px">
                    <div class="row no-gutters">
                      <div class="col-md-5">
                        <img src="http://image.tmdb.org/t/p/original${dt.poster_path}" class="card-img" alt="${dt.original_title}"/>
                      </div>
                      <div class="col-md-7"  style="text-align: left;">
                        <div class="card-body">
                          <p class="card-text"><b>Overview&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${dt.overview}</p>
                          <p class="card-text"><b>Popularity&nbsp&nbsp&nbsp: </b>${dt.popularity}</p>
                          <p class="card-text"><b>Release Date : </b>${dt.release_date}</p>
                          <p class="card-text"><b>Genres : </b>${gene}</p>
                          <p class="card-text"><b>Revenue&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${dt.revenue}$</p>
                          <p class="card-text"><b>Length&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${dt.runtime} minutes</p>
                          <p class="card-text"><b>Rated&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${dt.vote_average} (${dt.vote_count} vote)</p>
                          <p class="card-text"><b>Actor&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b> </p>
                          <p>${strActor.value}</p>
                          <p class="card-text"><b>Director&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>  ${strDirector.value} </p>                         
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row text-center py-2" style="margin : 10px 5px 0px; background: rgba(48, 155, 255, 0.164);">
                  <h2 class="col w-100">REVIEWS</h2>
              </div>
                  `);
                  fillReview(review);
}
async function evtGetActor(actorID)
{
    var status = $(`#status`);
    const apiActor = `https://api.themoviedb.org/3/person/${actorID}?api_key=${apikey}`;
    const apiCredit = `https://api.themoviedb.org/3/person/${actorID}/movie_credits?api_key=${apikey}`;
    loading();
    const resp = await fetch(apiActor); 
    const rs = await resp.json();
    const cre = await fetch(apiCredit); 
    const credit = await cre.json();
    status.empty();  
    status.append(rs.name);
    actorProfile(rs,credit);
}
function actorProfile(profile,credit)
{
  $('#review').empty(); $('#reviewBody').empty(); $('#rvPage').empty();  
  var gender = "Male";
  if (profile.gender == 1)
  {
    gender = "Female";
  }
  $('#main').empty();
  $('#main').append(`
  <div class="card border-info mb-3" style="margin: auto; padding : 5px">
                    <div class="row no-gutters">
                      <div class="col-md-5">
                        <img src="http://image.tmdb.org/t/p/original${profile.profile_path}" class="card-img" alt="${profile.profile_path}"/>
                      </div>
                      <div class="col-md-7"  style="text-align: left;">
                        <div class="card-body">
                          <p class="card-text"><b>Known For Department&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${profile.known_for_department}</p>
                          <p class="card-text"><b>Birth Day&nbsp&nbsp&nbsp: </b>${profile.birthday}</p>
                          <p class="card-text"><b>Gender : </b>${gender}</p>
                          <p class="card-text"><b>Popularity&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${profile.popularity}</p>
                          <p class="card-text"><b>Place of Birth&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${profile.place_of_birth}</p>
                          <p class="card-text"><b>Biography&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp: </b>${profile.biography}</p>                        
                        </div>
                      </div>
                    </div>
                  </div>
                  `);
                  for (x = 0; x < credit.cast.length && x < 9;x++) // Chi hien thi 9 film hot nhat
                  {        
                    $('#reviewBody').append(`  
                    <div class="col-md-4 py-1">    
                        <div class="card mb-3" style="max-width: 540px; max-height: 261px; " a href ="" onClick="evtChooseMovie(${credit.cast[x].id})">
                        <div class="row no-gutters">
                          <div class="col-md-6">
                            <img src="http://image.tmdb.org/t/p/original${credit.cast[x].poster_path}" class="card-img" alt="...">
                          </div>
                          <div class="col-md-6">
                            <div class="card-body">
                              <h5 class="card-title">${credit.cast[x].original_title}</h5>
                              <p class="card-text">Date : ${credit.cast[x].release_date}</p>
                              <p class="card-text">Lang : ${credit.cast[x].original_language}</p>
                              <p class="card-text">Popularity : ${credit.cast[x].popularity}</p>
                              <p class="card-text">Vote : ${credit.cast[x].vote_average} (${credit.cast[x].vote_count})</p>
                              <p class="card-text"><small class="text-muted">${credit.cast[x].character}</small></p>
                            </div>
                          </div>
                        </div>
                      </div>
                </div>
                  `);
                  }

  
  $('#review').append(`Actor movies`);
}
function fillReview(rs)
{  
  $('#review').empty(); $('#reviewBody').empty(); $('#rvPage').empty();
  if (rs.results.length == 0)
  {
    $('#review').append(`<p>No Reviews</p>`);
  }
  for (const x in rs.results)
  {
  $('#reviewBody').append(`
  <div class="card">
  <div class="card-header">
  <b>Author :    ${rs.results[x].author}</b>
  </div>
  <ul class="list-group list-group-flush">
  <li class="list-group-item"><b>Content:    ${rs.results[x].content}</li>
    <li class="list-group-item"><a href ='${rs.results[x].content}' target = "_blank">Read more<a></li>
  </ul>
</div>
<br>
  `);

  }
}